import decimal

from django.contrib.auth.models import User
from django.db import (models, transaction)
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    inn = models.CharField(max_length=12, db_index=True)
    balance = models.DecimalField(max_digits=10, decimal_places=2, default=0)

    def transfer_money(self, inn, amount):
        """
        Перечисление средств со счета по указанным реквизитам (по ИНН)
        """
        amount = decimal.Decimal(amount)
        with transaction.atomic():
            # Предварительная проверка уже была сделана в форме, однако там
            # она была для отображения ошибок. Здесь же выполняем проверки в
            # рамках транзакции
            if self.balance < amount:
                return False

            recepients = User.objects.filter(profile__inn=inn)
            if recepients.count() == 0:
                return False

            piece = amount / recepients.count()

            self.balance -= amount
            self.save(update_fields=['balance'])

            for recepient in recepients:
                recepient.profile.balance += piece
                recepient.profile.save(update_fields=['balance'])

            return True

    def __str__(self):
        return "%s %s <%s>" % (
        self.user.first_name, self.user.last_name, self.user.email)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()