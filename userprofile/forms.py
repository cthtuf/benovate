from django import forms
from django.contrib.auth.models import User


class MoneyTransferForm(forms.Form):
    """
    Форма перевода средств по номеру ИНН
    """
    sender = forms.IntegerField(label='Отправитель',
                                widget=forms.Select(
                                    choices=[(o.id, str(o)) for o in
                                             User.objects.all()]))
    receiver_inn = forms.CharField(label="ИНН получателя")
    amount = forms.DecimalField(label="Отправляемая сумма")

    def clean_receiver_inn(self):
        """
        Проверяем что ИНН присутствует в базе
        """
        receiver_inn = self.cleaned_data['receiver_inn']
        if User.objects.filter(profile__inn=receiver_inn).count() == 0:
            # тут не учитываем вариант что пользователь может сам себе отправить
            raise forms.ValidationError('Указанный ИНН %(inn)s не найден',
                                        code='invalid',
                                        params={
                                            'inn': receiver_inn
                                        })

    def clean_amount(self):
        """
        Проверяем что на счете достаточно средств
        """
        amount = self.cleaned_data['amount']
        sender_id = self.cleaned_data['sender']
        sender = User.objects.get(pk=sender_id)
        if sender.profile.balance < amount:
            raise forms.ValidationError('Недостаточно средств на счете',
                                        code='invalid')
