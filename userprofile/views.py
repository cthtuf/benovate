from django.contrib import messages
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views.generic.edit import FormView

from .forms import MoneyTransferForm
from .mixins import AjaxableResponseMixin


class MoneyTransferView(AjaxableResponseMixin, FormView):
    """
    Cтраница с формой для перечисления средств
    """
    template_name = 'money_transfer/money_transfer.html'
    form_class = MoneyTransferForm
    success_url = reverse_lazy('money-transfer-form')
    status_message = None

    def form_valid(self, form):
        sender = User.objects.get(pk=self.request.POST['sender'])
        if sender.profile.transfer_money(inn=self.request.POST['receiver_inn'],
                                         amount=self.request.POST['amount']):
            self.status_message = 'Сумма успешно перечислена'
            if not self.request.is_ajax():
                messages.success(self.request, self.status_message)
        return super(MoneyTransferView, self).form_valid(form)

    def form_invalid(self, form):
        self.status_message = 'Во время выполнения операции произошла ошибка'
        if not self.request.is_ajax():
            messages.error(self.request, self.status_message)
        return super(MoneyTransferView, self).form_invalid(form)
