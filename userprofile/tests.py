from django.test import TestCase
from django.contrib.auth.models import User


class MoneyTransferTestCase(TestCase):
    def setUp(self):
        user1 = User.objects.create_user('test_user1', email='1@mail.ru',
                                 password='89f8ad9asd')
        user1.profile.inn = '111111'
        user1.profile.save(update_fields=['inn'])

        user2 = User.objects.create_user('test_user2', email='2@mail.ru',
                                 password='89f8ad9asd')
        user2.profile.inn = '111111'
        user2.profile.save(update_fields=['inn'])

        user3 = User.objects.create_user('test_user3', email='3@mail.ru',
                                 password='89f8ad9asd')
        user3.profile.inn = '111113'
        user3.profile.save(update_fields=['inn'])

        user4 = User.objects.create_user('test_user4', email='4@mail.ru',
                                 password='89f8ad9asd')
        user4.profile.inn = '111114'
        user4.profile.save(update_fields=['inn'])

    def test_empty_profile_on_create(self):
        """
        Проверяем что для всех созданных пользователей была создана запись в
        таблице Profiles и значение balance у каждого равно нулю.
        """
        for user in User.objects.all():
            self.assertEqual(user.profile.balance, 0)

    def test_money_transfer(self):
        """
        Проверяем что метод модели корректно распределяет средства
        во время операции перевода
        """
        # Закинем немного средств на счет пользователя 1
        user1 = User.objects.get(email='1@mail.ru')
        user1.profile.balance = 1000
        user1.profile.save(update_fields=['balance'])

        # Отправим средства всем пользователям с ИНН = 111111 (т.е. себе и
        # пользователю 2
        self.assertTrue(user1.profile.transfer_money(inn='111111', amount=100))
        self.assertEqual(User.objects.get(email='1@mail.ru').profile.balance,
                         950)
        self.assertEqual(User.objects.get(email='2@mail.ru').profile.balance,
                         50)
        user1.profile.refresh_from_db()

        # Отправим средства пользователю с ИНН 111113 (придут только ему)
        self.assertTrue(user1.profile.transfer_money(inn='111113', amount=300))
        self.assertEqual(User.objects.get(email='1@mail.ru').profile.balance,
                         650)
        self.assertEqual(User.objects.get(email='3@mail.ru').profile.balance,
                         300)
        user1.profile.refresh_from_db()

        # Отправим средства пользователю с несуществующим в базе ИНН
        self.assertFalse(user1.profile.transfer_money(inn='111115', amount=50))
        self.assertEqual(User.objects.get(email='1@mail.ru').profile.balance,
                         650)
        user1.profile.refresh_from_db()

        # Отправим средств больше, чем есть на счете
        self.assertFalse(
            user1.profile.transfer_money(inn='111114', amount=1000))
        self.assertEqual(User.objects.get(email='1@mail.ru').profile.balance,
                         650)
        self.assertEqual(User.objects.get(email='4@mail.ru').profile.balance, 0)
