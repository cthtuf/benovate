from django.http import JsonResponse


class AjaxableResponseMixin(object):
    """
    Примесь для формы отправки средств. Отправляет ответ в виде json если был
    запрос через AJAX.
    """
    def form_valid(self, form):
        response = super(AjaxableResponseMixin, self).form_valid(form)
        if self.request.is_ajax():
            data = {
                'message': self.status_message,
                'success': True
            }
            return JsonResponse(data)
        else:
            return response

    def form_invalid(self, form):
        response = super(AjaxableResponseMixin, self).form_invalid(form)
        if self.request.is_ajax():
            data = {
                'message': self.status_message,
                'success': False,
                'errors': form.errors

            }
            return JsonResponse(data)
        else:
            return response
