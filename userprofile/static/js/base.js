function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$(function(){
    try {
        //Добавляем csrf-токен в заголовки ajax-запросов
        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                try {
                    $.jGrowl("Во время выполнения запроса произошла ошибка: " + textStatus + ": " + errorThrown, {
                        sticky: true,
                        group: 'error'
                    });
                } catch (e) { console.error(e.message) }
            },
        });

        //Показываем всплывающие сообщения, пришедшие от django-message
        //Настроены цвета для двух типов сообщений: success и error
        $('.message-jgrowl').each(function(){
            if($(this).hasClass('error')){
                $.jGrowl($(this).text(), {
                    sticky: true,
                    group: 'error'
                });
            } else {
                if($(this).hasClass('success')){
                    $.jGrowl($(this).text(), { group: 'success'});
                } else {
                    $.jGrowl($(this).text());
                }
            }
        });
    } catch (e){
        console.error('Something goes wrong: ' + e.message)
    }
});