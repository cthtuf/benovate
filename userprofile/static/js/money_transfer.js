var app = (function(){
    var version = '0.1',
        in_process = false,
        process_transfer = function(e){
            e.preventDefault();
            var form = $(this);
            $.ajax({
                url: $(this).attr('action'),
                method: $(this).attr('method'),
                data: $(this).serialize(),
                beforeSend : function(){
                    //Отменяем запрос если один уже отправлен
                    if (in_process) return false;
                    $(form).find('button[type=submit]').attr('disabled', 'disabled');
                    $(form).find('.form-control').removeClass('form-control-danger').parents('.form-group').removeClass('has-danger');
                    in_process = true;
                }
            }).done(function(rpo){
                try {
                    if(rpo.success == true){
                        $.jGrowl(rpo.message, { group: 'success'})
                        form[0].reset();
                    } else {
                        for(idx in rpo.errors){
                            $('#id_'+idx).addClass('form-control-danger').parents('.form-group').addClass('has-danger');
                            $.jGrowl(rpo.message.concat(": ", rpo.errors[idx]), { sticky: true, group: 'error' });
                        }
                    }
                } catch (e) { console.error(e.message) }
            }).always(function(){
                in_process = false;
                $(form).find('button[type=submit]').removeAttr('disabled');
            });
        },
        init = function(){
            $('#form_process_transfer').on('submit', process_transfer);
            console.info('Version: '+version);
        }
    return {
        'init': init,
    }
})();

$(function(){
    //Можно закоментировать, чтобы проверить работу без AJAX
    app.init()
})