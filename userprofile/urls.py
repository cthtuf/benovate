from django.conf.urls import url
from .views import MoneyTransferView


urlpatterns = [
    url(r'^money-transfer/$', MoneyTransferView.as_view(),
        name='money-transfer-form'),
]